﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DKP_Tracker.Migrations
{
    public partial class AddIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Raiders_Id",
                table: "Raiders",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
