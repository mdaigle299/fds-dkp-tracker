﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DKP_Tracker.Migrations.DKPAuditLogsDatabaseMigrations
{
    public partial class InitialMigrationAuditLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DKPLogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RaiderId = table.Column<Guid>(nullable: false),
                    Dkp = table.Column<int>(nullable: false),
                    Reason = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    IsAdjustment = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DKPLogs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
