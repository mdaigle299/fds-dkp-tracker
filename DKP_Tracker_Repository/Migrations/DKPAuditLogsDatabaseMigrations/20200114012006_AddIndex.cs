﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DKP_Tracker.Migrations.DKPAuditLogsDatabaseMigrations
{
    public partial class AddIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_DKPLogs_Id",
                table: "DKPLogs",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
