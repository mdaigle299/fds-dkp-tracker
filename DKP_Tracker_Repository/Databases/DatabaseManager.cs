﻿namespace DKP_Tracker_Repository.Databases
{
    public class DatabaseManager
    {
        public static IRaidersDatabase RaidersDatabase()
        {
            return new RaidersDatabase();
        }

        public static IDKPAuditLogsDatabase DKPAuditLogsDatabase()
        {
            return new DKPAuditLogsDatabase();
        }
    }
}
