﻿using DKP_Tracker_Repository.Models;
using System;
using System.Collections.Generic;

namespace DKP_Tracker_Repository.Databases
{
    public interface IRaidersDatabase
    {
        void Create(Raider raider);

        void AddDKPBatch(List<Raider> raiders, int dkp);

        void AddDKP(Raider raider, int dkp);

        void Remove(Raider raider);

        Raider Get(Guid id);

        Raider Get(string name);

        IEnumerable<Raider> GetAll();

        void CommitChanges();
    }
}
