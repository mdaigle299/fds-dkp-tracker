﻿using DKP_Tracker_Repository.Models;
using System;
using System.Collections.Generic;

namespace DKP_Tracker_Repository.Databases
{
    public interface IDKPAuditLogsDatabase
    {
        void Create(DKPAuditLog log);

        IEnumerable<DKPAuditLog> GetLogsByRaider(Raider raider);

        IEnumerable<DKPAuditLog> GetLogsByDate(DateTime start, DateTime end, bool onlyAdjustment);

        int GetDKP(Raider raider);
    }
}
