﻿using DKP_Tracker_Repository.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DKP_Tracker_Repository.Databases
{
    public class RaidersDatabase : DbContext, IRaidersDatabase
    {
        public DbSet<Raider> Raiders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseNpgsql(Configuration.Configuration.Instance().GetConnectionConfig().GetPostgreSQL());

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Raider>()
                   .HasIndex(x => x.Id);

            base.OnModelCreating(modelBuilder);
        }

        public void Create(Raider raider)
        {
            var foundRaider = Get(raider.Id);

            if (foundRaider == null)
            {
                Raiders.Add(raider);
            }

            SaveChanges();
        }

        public void AddDKPBatch(List<Raider> raiders, int dkp)
        {
            foreach (var raider in raiders)
            {
                var foundRaider = Get(raider.Id);

                if (foundRaider != null)
                {
                    //foundRaider.DKP += dkp;
                    Raiders.Update(foundRaider);
                }
            }

            SaveChangesAsync();
        }

        public void AddDKP(Raider raider, int dkp)
        {
            var foundRaider = Get(raider.Id);

            if (foundRaider != null)
            {
                //foundRaider.DKP += dkp;
                Raiders.Update(foundRaider);
            }

            SaveChangesAsync();
        }

        public void CommitChanges()
        {
            SaveChangesAsync();
        }

        public void Remove(Raider raider)
        {
            Remove(raider);
            SaveChangesAsync();
        }

        public Raider Get(Guid id)
        {
            var raider = Raiders.Find(id);

            if (raider == null || raider.Deleted)
            {
                return null;
            }

            return raider;
        }

        public Raider Get(string name)
        {
            var raider = Raiders.Where(x => x.Name == name);

            if (!raider.Any() || raider.First().Deleted)
            {
                return null;
            }

            return raider.First();
        }

        public IEnumerable<Raider> GetAll()
        {
            return Raiders.AsEnumerable().Where(x => !x.Deleted).ToList();
        }
    }
}
