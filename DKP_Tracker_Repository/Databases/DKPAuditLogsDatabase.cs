﻿using DKP_Tracker_Repository.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace DKP_Tracker_Repository.Databases
{
    public class DKPAuditLogsDatabase : DbContext, IDKPAuditLogsDatabase
    {
        public DbSet<DKPAuditLog> DKPLogs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseNpgsql(Configuration.Configuration.Instance().GetConnectionConfig().GetPostgreSQL());

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DKPAuditLog>()
                .HasIndex(x => x.Id);

            base.OnModelCreating(modelBuilder);
        }

        public void Create(DKPAuditLog log)
        {
            DKPLogs.Add(log);
            SaveChangesAsync();
        }

        public IEnumerable<DKPAuditLog> GetLogsByRaider(Raider raider)
        {
            var logs = DKPLogs.Where(x => x.RaiderId == raider.Id);

            return logs.OrderBy(x => x.Date); ;
        }

        public IEnumerable<DKPAuditLog> GetLogsByDate(DateTime start, DateTime end, bool onlyAdjustment)
        {
            IEnumerable<DKPAuditLog> logs;
            if (onlyAdjustment)
            {
                logs = DKPLogs.Where(x => x.Date >= start && x.Date < end && x.IsAdjustment == onlyAdjustment);
            }
            else
            {
                logs = DKPLogs.Where(x => x.Date >= start && x.Date < end);
            }

            return logs.OrderBy(x => x.Date);
        }

        public int GetDKP(Raider raider)
        {
            return DKPLogs.Where(x => x.RaiderId == raider.Id)
                .Sum(x => x.Dkp);
        }
    }
}
