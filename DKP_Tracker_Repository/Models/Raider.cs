﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DKP_Tracker_Repository.Models
{
    public class Raider
    {
        [Required]
        public Guid Id { get; }

        public string Name { get; set; }

        public int DKP { get; set;  }

        public string RClass { get; set; }

        public bool Deleted { get; set; }
        
        public Rank Rank { get; set; }

        public Raider(string name, string rClass)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("A Raider's name can't be null or empty");
            }

            Id = Guid.NewGuid();
            Name = name;
            DKP = 0;
            RClass = rClass;
            Deleted = false;
        }

        public override string ToString()
        {
            return $"{Name} - {RClass}";
        }

        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }

            if (obj is Raider raider && raider.ToString().Equals(this.ToString()))
            {
                return true;
            }

            if (obj is string str && this.ToString().Equals(str))
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, DKP, RClass);
        }

        public static int SortByDKPDesc(Raider x, Raider y)
        {
            if (x.DKP == y.DKP)
            {
                return 0;
            }

            if (x.DKP > y.DKP)
            {
                return 1;
            }

            return 1;
        }
    }
}
