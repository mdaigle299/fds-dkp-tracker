﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DKP_Tracker_Repository.Models
{
    public class DKPAuditLog
    {
        [Required]
        public Guid Id { get; }

        public Guid RaiderId { get; set; }

        public int Dkp { get; set; }
        
        public string Reason { get; set; }

        public DateTime Date { get; set; }

        public bool IsAdjustment { get; set; }

        public DKPAuditLog(Guid raiderId, int dkp, string reason, bool isAdjustment)
        {
            if (string.IsNullOrEmpty(reason))
            {
                throw new Exception("You must provide a reason");
            }

            Id = Guid.NewGuid();
            Date = DateTime.Now;
            RaiderId = raiderId;
            Dkp = dkp;
            Reason = reason;
            IsAdjustment = isAdjustment;
        }
    }
}
