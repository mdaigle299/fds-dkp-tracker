﻿using System.Collections.Generic;

namespace DKP_Tracker_Repository.Models
{
    public class Classes
    {
        static string Mage = "Mage";

        static string Priest = "Priest";

        static string Shaman = "Shaman";

        static string Warrior = "Warrior";

        static string Rogue = "Rogue";

        static string Warlock = "Warlock";

        static string Hunter = "Hunter";

        static string Paladin = "Paladin";

        static string Druid = "Druid";

        public static IEnumerable<string> GetAllClasses()
        {
            return new string[9]
            {
                Mage,
                Priest,
                Shaman,
                Warrior,
                Rogue,
                Warlock,
                Hunter,
                Paladin,
                Druid
            };
        }
    }
}
