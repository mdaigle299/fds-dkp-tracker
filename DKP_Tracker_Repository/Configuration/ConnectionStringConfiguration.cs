﻿namespace DKP_Tracker_Repository.Configuration
{
    public class ConnectionStringConfiguration
    {
        public string UserId { get; set; }

        public string Password { get; set; }

        public string Host { get; set; }

        public string Port { get; set; }

        public string Database { get; set; }

        public string GetPostgreSQL()
        {
            return $"User ID={UserId};Password={Password};Host={Host};Port={Port};Database={Database};Trust Server Certificate=true;Integrated Security=false;Tcp Keepalive=true;pooling=false";
        }

        public ConnectionStringConfiguration()
        {
        }
    }
}
