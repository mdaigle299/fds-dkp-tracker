﻿using Newtonsoft.Json.Linq;

namespace DKP_Tracker_Repository.Configuration
{
    public class Configuration
    {
        private static Configuration _instance;

        const string AppSettingFile = "appsetting.json";

        private ConnectionStringConfiguration connectionStringConfiguration;

        private string iconPath = null;

        private JObject jsonObject;

        private Configuration()
        {
            jsonObject = JObject.Parse(LoadJson());
        }

        public static Configuration Instance()
        {
            if (_instance == null)
            {
                _instance = new Configuration();
            }

            return _instance;
        }

        private string LoadJson()
        {
            return System.IO.File.ReadAllText("./" + AppSettingFile);
        }

        public ConnectionStringConfiguration GetConnectionConfig()
        {
            if (connectionStringConfiguration == null)
            {
                var token = jsonObject["ConnectionSettings"];

                connectionStringConfiguration = token.ToObject<ConnectionStringConfiguration>();
            }

            return connectionStringConfiguration;
        }

        public string GetIconPath()
        {
            if (iconPath == null)
            {
                var token = jsonObject["IconPath"];

                iconPath = token.ToObject<string>();
            }

            return iconPath;
        }
    }
}
