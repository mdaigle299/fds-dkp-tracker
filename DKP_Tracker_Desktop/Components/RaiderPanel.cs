﻿using DKP_Tracker_Repository.Databases;
using DKP_Tracker_Repository.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace DKP_Tracker_Desktop.Components
{
    public class RaiderPanel
    {
        private Label RaiderNameLabel = new Label();
        private Label RaiderClassLabel = new Label();
        private Label RaiderDkpLabel = new Label();
        private Button RaiderSelectButton = new Button();
        
        static readonly int name_offset = 0;
        static readonly int class_offset = 120;
        static readonly int dkp_offset = 190;
        static readonly int select_offset = 275;

        private KeyValuePair<Raider, int>? CurrentRaider = null;
        private Action<string> SelectRaider;

        public RaiderPanel(Control parent, Action<string> selectRaider, int x, int y)
        {
            SelectRaider = selectRaider;

            Font font = new Font("Segoe UI", 12);

            this.RaiderNameLabel.Location = new Point(x + name_offset, y);
            this.RaiderNameLabel.Parent = parent;
            this.RaiderNameLabel.Width = 110;
            this.RaiderNameLabel.Font = font;

            this.RaiderClassLabel.Location = new Point(x + class_offset, y);
            this.RaiderClassLabel.Parent = parent;
            this.RaiderClassLabel.Width = 70;
            this.RaiderClassLabel.Font = font;

            this.RaiderDkpLabel.Location = new Point(x + dkp_offset, y);
            this.RaiderDkpLabel.Parent = parent;
            this.RaiderDkpLabel.Width = 75;
            this.RaiderDkpLabel.TextAlign = ContentAlignment.TopRight;
            this.RaiderDkpLabel.AutoSize = false;
            this.RaiderDkpLabel.Font = font;

            this.RaiderSelectButton.Location = new Point(x + select_offset, y);
            this.RaiderSelectButton.Text = "Select";
            this.RaiderSelectButton.Parent = parent;
            this.RaiderSelectButton.Width = 50;
            this.RaiderSelectButton.Click += Select_Button_Click;

            this.Hide();
        }

        public void Show(KeyValuePair<Raider, int> raider)
        {
            SetPanel(raider);

            RaiderNameLabel.Show();
            RaiderClassLabel.Show();
            RaiderDkpLabel.Show();
            RaiderSelectButton.Show();
        }

        public void Hide()
        {
            CurrentRaider = null;

            RaiderNameLabel.Hide();
            RaiderClassLabel.Hide();
            RaiderDkpLabel.Hide();
            RaiderSelectButton.Hide();
        }

        public void SetPanel(KeyValuePair<Raider, int> raider)
        {
            CurrentRaider = raider;

            this.RaiderNameLabel.Text = CurrentRaider?.Key.Name;
            this.RaiderClassLabel.Text = CurrentRaider?.Key.RClass;
            this.RaiderDkpLabel.Text = CurrentRaider?.Value.ToString();
        }

        private void Select_Button_Click(object sender, System.EventArgs e)
        {
            if(CurrentRaider != null && SelectRaider != null)
            { 
                SelectRaider(CurrentRaider?.Key.Name);
            }
        }

    }
}
