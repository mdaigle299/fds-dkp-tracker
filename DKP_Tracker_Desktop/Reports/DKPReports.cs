﻿using DKP_Tracker_Repository.Models;
using DKP_Tracker_Repository.Databases;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DKP_Tracker_Desktop.Reports
{
    class DKPReports
    {
        public static string CreateDetailedDKPReport(Raider raider, IEnumerable<DKPAuditLog> logs)
        {
            var report = $"--- {raider.Name} Detailed report Current DKP {DatabaseManager.DKPAuditLogsDatabase().GetDKP(raider)}\r\n";

            foreach (var log in logs)
            {
                report += $"  --- {log.Date.ToString("MM/dd/yyyy HH:mm")} --- Adjustment : {log.Dkp} Reason : {log.Reason}\r\n";
            }

            return report;
        }

        public static string CreateDatedReport(IEnumerable<DKPAuditLog> logs, IEnumerable<Raider> raiders, DateTime start, DateTime end)
        {
            var report = $"--- Detailed report for Adjustment for {start.ToString("yyyy/MM/dd")} - {end.ToString("yyyy/MM/dd")}\r\n";

            foreach (var log in logs)
            {
                var raider = raiders.Where(x => x.Id == log.RaiderId);

                if (raider.Any())
                {
                    report += $"  --- {log.Date.ToString("yyyy/MM/dd HH:mm")} --- Raider : {raider.First().Name} Adjustment : {log.Dkp} Reason : {log.Reason}\r\n";
                }
            }

            return report;
        }

        public static string CreateReport(string format, IEnumerable<Raider> currentRaiders)
        {
            var raiders = DatabaseManager.RaidersDatabase().GetAll().ToList();
            var report = "";

            var sortOptions = new SortOptions(format);

            format = format.Replace("%OrderByDkp%", "");
            format = format.Replace("%Desc%", "");
            format = format.Replace("%OrderByClass%", "");
            format = format.Replace("%OrderByName%", "");

            Dictionary<Raider, int> raidersDkp = new Dictionary<Raider, int>(raiders.Count);

            foreach (var raider in raiders)
            {
                raidersDkp.Add(raider, DatabaseManager.DKPAuditLogsDatabase().GetDKP(raider));
            }

            if (!sortOptions.Ascendant)
            {
                if (sortOptions.Type == SortOptions.SortType.Class)
                {
                    raiders.Sort(delegate (Raider x, Raider y)
                    {
                        var classcompare = x.RClass.CompareTo(y.RClass);

                        if (classcompare != 0)
                        {
                            return -classcompare;
                        }

                        if (raidersDkp[x] == raidersDkp[y])
                        {
                            return 0;
                        }

                        if (raidersDkp[x] > raidersDkp[y])
                        {
                            return -1;
                        }

                        return 1;
                    });
                }
                else if (sortOptions.Type == SortOptions.SortType.Dkp)
                {
                    raiders.Sort(delegate (Raider x, Raider y)
                    {
                        if (raidersDkp[x] == raidersDkp[y])
                        {
                            return 0;
                        }

                        if (raidersDkp[x] > raidersDkp[y])
                        {
                            return 1;
                        }

                        return -1;
                    });
                }
                else
                {
                    raiders.Sort(delegate (Raider x, Raider y)
                    {
                        return -x.Name.CompareTo(y.Name);
                    });
                }
            }
            else
            {
                if (sortOptions.Type == SortOptions.SortType.Class)
                {

                    raiders.Sort(delegate (Raider x, Raider y)
                    {
                        var classcompare = x.RClass.CompareTo(y.RClass);

                        if (classcompare != 0)
                        {
                            return classcompare;
                        }

                        if (raidersDkp[x] == raidersDkp[y])
                        {
                            return 0;
                        }

                        if (raidersDkp[x] > raidersDkp[y])
                        {
                            return -1;
                        }

                        return 1;
                    });
                }
                else if (sortOptions.Type == SortOptions.SortType.Dkp)
                {
                    raiders.Sort(delegate (Raider x, Raider y)
                    {
                        if (raidersDkp[x] == raidersDkp[y])
                        {
                            return 0;
                        }

                        if (raidersDkp[x] > raidersDkp[y])
                        {
                            return -1;
                        }

                        return 1;
                    });
                }
                else
                {
                    raiders.Sort(delegate (Raider x, Raider y)
                    {
                        return x.Name.CompareTo(y.Name);
                    });
                }
            }

            format = format.Replace("%OrderByDkp%", "");
            format = format.Replace("%Desc%", "");
            format = format.Replace("%OrderByClass%", "");
            format = format.Replace("%OrderByName%", "");

            var currentClass = string.Empty;

            report = ":Logo: Grands Frères / Grandes Soeurs :Logo:\r\n";

            foreach (var raider in raiders)
            {
                if (raider.Rank == Rank.Core)
                {
                    var patate = $"{format}\r\n";
                    if (sortOptions.Type == SortOptions.SortType.Class)
                    {
                        if (currentClass != raider.RClass)
                        {
                            report += $":{raider.RClass}:\r\n";
                            currentClass = raider.RClass;
                        }

                        patate = patate.Replace("%Class%", "");
                    }
                    else
                    {
                        patate = patate.Replace("%Class%", raider.RClass);
                    }

                    patate = patate.Replace("%Presence%", currentRaiders.Any(x => x.Name == raider.Name) ? "présent" : "absent");
                    patate = patate.Replace("%Name%", raider.Name);
                    patate = patate.Replace("%Dkp%", raidersDkp[raider].ToString());

                    report += patate;
                }
            }

            report += "\r\n:MVP: Frères / Soeurs :MVP:\r\n";

            foreach (var raider in raiders)
            {
                if (raider.Rank == Rank.Raider)
                {
                    var patate = $"{format}\r\n";
                    if (sortOptions.Type == SortOptions.SortType.Class)
                    {
                        if (currentClass != raider.RClass)
                        {
                            report += $":{raider.RClass}:\r\n";
                            currentClass = raider.RClass;
                        }

                        patate = patate.Replace("%Class%", "");
                    }
                    else
                    {
                        patate = patate.Replace("%Class%", raider.RClass);
                    }

                    patate = patate.Replace("%Presence%", currentRaiders.Any(x => x.Name == raider.Name) ? "présent" : "absent");
                    patate = patate.Replace("%Name%", raider.Name);
                    patate = patate.Replace("%Dkp%", raidersDkp[raider].ToString());

                    report += patate;
                }
            }

            return report;
        }

        internal class SortOptions
        {
            public SortType Type { get; }

            public bool Ascendant { get; }

            public SortOptions(string format)
            {
                var sort_dkp = format.Contains("%OrderByDkp%");
                var desc = format.Contains("%Desc%");
                var sort_class = format.Contains("%OrderByClass%");
                //var sort_name = format.Contains("%OrderByName%");

                if (sort_dkp)
                {
                    Type = SortType.Dkp;
                }
                else if (sort_class)
                {
                    Type = SortType.Class;
                }
                else
                {
                    Type = SortType.Name;
                }

                Ascendant = !desc;
            }
            internal enum SortType
            {
                Dkp,
                Class,
                Name
            }
        }


    }

}
