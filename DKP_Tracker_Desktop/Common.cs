﻿using DKP_Tracker_Desktop.Forms;

namespace DKP_Tracker_Desktop
{
    public class Common
    {
        public static string GetReason()
        {
            // Add real reason
            var modal = new ReasonModal();
            modal.ShowDialog();

            if (!modal.Cancelled)
            {
                return modal.GetReason();
            }

            return null;
        }

    }
}
