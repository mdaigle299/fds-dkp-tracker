﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;

namespace DKP_Tracker_Desktop.Utils
{
    class JsonUtils
    {
        public static void SaveToJson(List<string> collection, string filename)
        {
            File.WriteAllText($"./{filename}.json", JsonConvert.SerializeObject(collection));
        }

        public static List<string> LoadFromJson(string filename)
        {
            var items = new List<string>();

            try
            {
                var jsonString = File.ReadAllText($"./{filename}.json");
                items = JsonConvert.DeserializeObject<List<string>>(jsonString);
            }
            catch (FileNotFoundException)
            {
            }

            return items;
        }
    }
}
