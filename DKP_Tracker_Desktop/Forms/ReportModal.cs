﻿using DKP_Tracker_Repository.Configuration;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace DKP_Tracker_Desktop.Forms
{
    public partial class ReportModal : Form
    {
        public bool Cancelled = true;

        public ReportModal()
        {
            this.Icon = new System.Drawing.Icon(Configuration.Instance().GetIconPath());

            InitializeComponent();
        }

        public string GetReportString()
        {
            return Reason_Text.Text;
        }

        private void Cofirm_Button_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(Reason_Text.Text))
            {
                Feedback("You need to provide a report that is not empty", Color.DarkRed);
            }
            else
            {
                Cancelled = false;
                Close();
            }
        }

        private void Cancel_Button_Click(object sender, EventArgs e)
        {
            Cancelled = true;
            Close();
        }

        private void Feedback(string message, Color color)
        {
            Feedback_Label.Text = message;
            Feedback_Label.ForeColor = color;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Reason_Text_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
