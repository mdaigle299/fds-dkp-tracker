﻿namespace DKP_Tracker_Desktop.Forms
{
    partial class DKPAdjustment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Filter_Druid_Button = new System.Windows.Forms.Button();
            this.Filter_Hunter_Button = new System.Windows.Forms.Button();
            this.Filter_Mage_Button = new System.Windows.Forms.Button();
            this.Filter_Paladin_Button = new System.Windows.Forms.Button();
            this.Filter_Priest_Button = new System.Windows.Forms.Button();
            this.Filter_Rogue_Button = new System.Windows.Forms.Button();
            this.Filter_Shaman_Button = new System.Windows.Forms.Button();
            this.Filter_Warlock_Button = new System.Windows.Forms.Button();
            this.Filter_Warrior_Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.Raider_Combobox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Adjust_DKP_Numeric = new System.Windows.Forms.NumericUpDown();
            this.Sign_Checkbox = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.Show_All_Button = new System.Windows.Forms.Button();
            this.Hide_All_Button = new System.Windows.Forms.Button();
            this.Refresh_Button = new System.Windows.Forms.Button();
            this.Previous_Button = new System.Windows.Forms.Button();
            this.Next_Button = new System.Windows.Forms.Button();
            this.Page_Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Adjust_DKP_Numeric)).BeginInit();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 21);
            this.label1.TabIndex = 4;
            this.label1.Text = "Filters";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Filter_Druid_Button
            // 
            this.Filter_Druid_Button.Location = new System.Drawing.Point(15, 40);
            this.Filter_Druid_Button.Name = "Filter_Druid_Button";
            this.Filter_Druid_Button.Size = new System.Drawing.Size(140, 25);
            this.Filter_Druid_Button.TabIndex = 5;
            this.Filter_Druid_Button.Text = "Druid : SHOW";
            this.Filter_Druid_Button.UseVisualStyleBackColor = true;
            this.Filter_Druid_Button.Click += new System.EventHandler(this.Class_Filter_Click);
            this.Filter_Druid_Button.Paint += new System.Windows.Forms.PaintEventHandler(this.Class_Filter_Paint);
            // 
            // Filter_Hunter_Button
            // 
            this.Filter_Hunter_Button.Location = new System.Drawing.Point(15, 75);
            this.Filter_Hunter_Button.Name = "Filter_Hunter_Button";
            this.Filter_Hunter_Button.Size = new System.Drawing.Size(140, 25);
            this.Filter_Hunter_Button.TabIndex = 5;
            this.Filter_Hunter_Button.Text = "Hunter : SHOW";
            this.Filter_Hunter_Button.UseVisualStyleBackColor = true;
            this.Filter_Hunter_Button.Click += new System.EventHandler(this.Class_Filter_Click);
            this.Filter_Hunter_Button.Paint += new System.Windows.Forms.PaintEventHandler(this.Class_Filter_Paint);
            // 
            // Filter_Mage_Button
            // 
            this.Filter_Mage_Button.Location = new System.Drawing.Point(15, 110);
            this.Filter_Mage_Button.Name = "Filter_Mage_Button";
            this.Filter_Mage_Button.Size = new System.Drawing.Size(140, 25);
            this.Filter_Mage_Button.TabIndex = 5;
            this.Filter_Mage_Button.Text = "Mage : SHOW";
            this.Filter_Mage_Button.UseVisualStyleBackColor = true;
            this.Filter_Mage_Button.Click += new System.EventHandler(this.Class_Filter_Click);
            this.Filter_Mage_Button.Paint += new System.Windows.Forms.PaintEventHandler(this.Class_Filter_Paint);
            // 
            // Filter_Paladin_Button
            // 
            this.Filter_Paladin_Button.Location = new System.Drawing.Point(15, 145);
            this.Filter_Paladin_Button.Name = "Filter_Paladin_Button";
            this.Filter_Paladin_Button.Size = new System.Drawing.Size(140, 25);
            this.Filter_Paladin_Button.TabIndex = 5;
            this.Filter_Paladin_Button.Text = "Paladin : SHOW";
            this.Filter_Paladin_Button.UseVisualStyleBackColor = true;
            this.Filter_Paladin_Button.Click += new System.EventHandler(this.Class_Filter_Click);
            this.Filter_Paladin_Button.Paint += new System.Windows.Forms.PaintEventHandler(this.Class_Filter_Paint);
            // 
            // Filter_Priest_Button
            // 
            this.Filter_Priest_Button.Location = new System.Drawing.Point(15, 180);
            this.Filter_Priest_Button.Name = "Filter_Priest_Button";
            this.Filter_Priest_Button.Size = new System.Drawing.Size(140, 25);
            this.Filter_Priest_Button.TabIndex = 5;
            this.Filter_Priest_Button.Text = "Priest : SHOW";
            this.Filter_Priest_Button.UseVisualStyleBackColor = true;
            this.Filter_Priest_Button.Click += new System.EventHandler(this.Class_Filter_Click);
            this.Filter_Priest_Button.Paint += new System.Windows.Forms.PaintEventHandler(this.Class_Filter_Paint);
            // 
            // Filter_Rogue_Button
            // 
            this.Filter_Rogue_Button.Location = new System.Drawing.Point(15, 215);
            this.Filter_Rogue_Button.Name = "Filter_Rogue_Button";
            this.Filter_Rogue_Button.Size = new System.Drawing.Size(140, 25);
            this.Filter_Rogue_Button.TabIndex = 5;
            this.Filter_Rogue_Button.Text = "Rogue : SHOW";
            this.Filter_Rogue_Button.UseVisualStyleBackColor = true;
            this.Filter_Rogue_Button.Click += new System.EventHandler(this.Class_Filter_Click);
            this.Filter_Rogue_Button.Paint += new System.Windows.Forms.PaintEventHandler(this.Class_Filter_Paint);
            // 
            // Filter_Shaman_Button
            // 
            this.Filter_Shaman_Button.Location = new System.Drawing.Point(15, 250);
            this.Filter_Shaman_Button.Name = "Filter_Shaman_Button";
            this.Filter_Shaman_Button.Size = new System.Drawing.Size(140, 25);
            this.Filter_Shaman_Button.TabIndex = 5;
            this.Filter_Shaman_Button.Text = "Shaman : SHOW";
            this.Filter_Shaman_Button.UseVisualStyleBackColor = true;
            this.Filter_Shaman_Button.Click += new System.EventHandler(this.Class_Filter_Click);
            this.Filter_Shaman_Button.Paint += new System.Windows.Forms.PaintEventHandler(this.Class_Filter_Paint);
            // 
            // Filter_Warlock_Button
            // 
            this.Filter_Warlock_Button.Location = new System.Drawing.Point(15, 285);
            this.Filter_Warlock_Button.Name = "Filter_Warlock_Button";
            this.Filter_Warlock_Button.Size = new System.Drawing.Size(140, 25);
            this.Filter_Warlock_Button.TabIndex = 5;
            this.Filter_Warlock_Button.Text = "Warlock : SHOW";
            this.Filter_Warlock_Button.UseVisualStyleBackColor = true;
            this.Filter_Warlock_Button.Click += new System.EventHandler(this.Class_Filter_Click);
            this.Filter_Warlock_Button.Paint += new System.Windows.Forms.PaintEventHandler(this.Class_Filter_Paint);
            // 
            // Filter_Warrior_Button
            // 
            this.Filter_Warrior_Button.Location = new System.Drawing.Point(15, 320);
            this.Filter_Warrior_Button.Name = "Filter_Warrior_Button";
            this.Filter_Warrior_Button.Size = new System.Drawing.Size(140, 25);
            this.Filter_Warrior_Button.TabIndex = 5;
            this.Filter_Warrior_Button.Text = "Warrior : SHOW";
            this.Filter_Warrior_Button.UseVisualStyleBackColor = true;
            this.Filter_Warrior_Button.Click += new System.EventHandler(this.Class_Filter_Click);
            this.Filter_Warrior_Button.Paint += new System.Windows.Forms.PaintEventHandler(this.Class_Filter_Paint);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(172, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(340, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "Raiders";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Raider_Combobox
            // 
            this.Raider_Combobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Raider_Combobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Raider_Combobox.Items.AddRange(new object[] {
            "Patate",
            "au",
            "fou",
            "Pascal"});
            this.Raider_Combobox.Location = new System.Drawing.Point(660, 42);
            this.Raider_Combobox.Name = "Raider_Combobox";
            this.Raider_Combobox.Size = new System.Drawing.Size(121, 23);
            this.Raider_Combobox.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(605, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "Adjustment";
            // 
            // Adjust_DKP_Numeric
            // 
            this.Adjust_DKP_Numeric.Location = new System.Drawing.Point(535, 71);
            this.Adjust_DKP_Numeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.Adjust_DKP_Numeric.Name = "Adjust_DKP_Numeric";
            this.Adjust_DKP_Numeric.Size = new System.Drawing.Size(112, 23);
            this.Adjust_DKP_Numeric.TabIndex = 0;
            // 
            // Sign_Checkbox
            // 
            this.Sign_Checkbox.AutoSize = true;
            this.Sign_Checkbox.Checked = true;
            this.Sign_Checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Sign_Checkbox.Location = new System.Drawing.Point(535, 44);
            this.Sign_Checkbox.Name = "Sign_Checkbox";
            this.Sign_Checkbox.Size = new System.Drawing.Size(89, 19);
            this.Sign_Checkbox.TabIndex = 8;
            this.Sign_Checkbox.Text = "Negative (-)";
            this.Sign_Checkbox.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(660, 71);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Adjust DKP";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Adjust_DKP_Button_Click);
            // 
            // Show_All_Button
            // 
            this.Show_All_Button.Location = new System.Drawing.Point(15, 435);
            this.Show_All_Button.Name = "Show_All_Button";
            this.Show_All_Button.Size = new System.Drawing.Size(123, 23);
            this.Show_All_Button.TabIndex = 2;
            this.Show_All_Button.Text = "Show All";
            this.Show_All_Button.UseVisualStyleBackColor = true;
            this.Show_All_Button.Click += new System.EventHandler(this.Show_All_Button_Click);
            // 
            // Hide_All_Button
            // 
            this.Hide_All_Button.Location = new System.Drawing.Point(144, 435);
            this.Hide_All_Button.Name = "Hide_All_Button";
            this.Hide_All_Button.Size = new System.Drawing.Size(123, 23);
            this.Hide_All_Button.TabIndex = 2;
            this.Hide_All_Button.Text = "Hide All";
            this.Hide_All_Button.UseVisualStyleBackColor = true;
            this.Hide_All_Button.Click += new System.EventHandler(this.Hide_All_Button_Click);
            // 
            // Refresh_Button
            // 
            this.Refresh_Button.Location = new System.Drawing.Point(658, 435);
            this.Refresh_Button.Name = "Refresh_Button";
            this.Refresh_Button.Size = new System.Drawing.Size(123, 23);
            this.Refresh_Button.TabIndex = 2;
            this.Refresh_Button.Text = "Refresh";
            this.Refresh_Button.UseVisualStyleBackColor = true;
            this.Refresh_Button.Click += new System.EventHandler(this.Refresh_Button_Click);
            // 
            // Previous_Button
            // 
            this.Previous_Button.Location = new System.Drawing.Point(172, 406);
            this.Previous_Button.Name = "Previous_Button";
            this.Previous_Button.Size = new System.Drawing.Size(75, 23);
            this.Previous_Button.TabIndex = 10;
            this.Previous_Button.Text = "< Previous";
            this.Previous_Button.UseVisualStyleBackColor = true;
            this.Previous_Button.Click += new System.EventHandler(this.Previous_Button_Click);
            // 
            // Next_Button
            // 
            this.Next_Button.Location = new System.Drawing.Point(437, 406);
            this.Next_Button.Name = "Next_Button";
            this.Next_Button.Size = new System.Drawing.Size(75, 23);
            this.Next_Button.TabIndex = 11;
            this.Next_Button.Text = "Next >";
            this.Next_Button.UseVisualStyleBackColor = true;
            this.Next_Button.Click += new System.EventHandler(this.button3_Click);
            // 
            // Page_Label
            // 
            this.Page_Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Page_Label.Location = new System.Drawing.Point(172, 407);
            this.Page_Label.Name = "Page_Label";
            this.Page_Label.Size = new System.Drawing.Size(340, 21);
            this.Page_Label.TabIndex = 4;
            this.Page_Label.Text = "0";
            this.Page_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Page_Label.Click += new System.EventHandler(this.label4_Click);
            // 
            // DKPAdjustment
            // 
            this.ClientSize = new System.Drawing.Size(812, 463);
            this.Controls.Add(this.Next_Button);
            this.Controls.Add(this.Previous_Button);
            this.Controls.Add(this.Filter_Druid_Button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Filter_Hunter_Button);
            this.Controls.Add(this.Filter_Mage_Button);
            this.Controls.Add(this.Filter_Paladin_Button);
            this.Controls.Add(this.Filter_Priest_Button);
            this.Controls.Add(this.Filter_Rogue_Button);
            this.Controls.Add(this.Filter_Shaman_Button);
            this.Controls.Add(this.Filter_Warlock_Button);
            this.Controls.Add(this.Filter_Warrior_Button);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Raider_Combobox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Adjust_DKP_Numeric);
            this.Controls.Add(this.Sign_Checkbox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Show_All_Button);
            this.Controls.Add(this.Hide_All_Button);
            this.Controls.Add(this.Refresh_Button);
            this.Controls.Add(this.Page_Label);
            this.Name = "DKPAdjustment";
            this.Load += new System.EventHandler(this.DKPAdjustment_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.DKPAdjustment_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.Adjust_DKP_Numeric)).EndInit();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Filter_Mage_Button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Filter_Druid_Button;
        private System.Windows.Forms.Button Filter_Hunter_Button;
        private System.Windows.Forms.Button Filter_Paladin_Button;
        private System.Windows.Forms.Button Filter_Priest_Button;
        private System.Windows.Forms.Button Filter_Rogue_Button;
        private System.Windows.Forms.Button Filter_Shaman_Button;
        private System.Windows.Forms.Button Filter_Warlock_Button;
        private System.Windows.Forms.Button Filter_Warrior_Button;
        private System.Windows.Forms.ComboBox Raider_Combobox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown Adjust_DKP_Numeric;
        private System.Windows.Forms.CheckBox Sign_Checkbox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Show_All_Button;
        private System.Windows.Forms.Button Hide_All_Button;
        private System.Windows.Forms.Button Refresh_Button;
        private System.Windows.Forms.Button Previous_Button;
        private System.Windows.Forms.Button Next_Button;
        private System.Windows.Forms.Label Page_Label;
    }
}