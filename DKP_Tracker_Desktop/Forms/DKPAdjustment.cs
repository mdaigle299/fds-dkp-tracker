﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using System;
using DKP_Tracker_Desktop.Components;
using DKP_Tracker_Repository.Models;
using DKP_Tracker_Repository.Configuration;
using DKP_Tracker_Repository.Databases;

namespace DKP_Tracker_Desktop.Forms
{
    public partial class DKPAdjustment : Form
    {
        private const int NUMBER_SHOWN = 10;

        private Dictionary<string, bool> Filters = new Dictionary<string, bool>();

        private List<RaiderPanel> RaiderPanels = new List<RaiderPanel>(NUMBER_SHOWN);

        private Dictionary<Raider, int> Raiders = new Dictionary<Raider, int>(40);

        private int Page = 0;

        public DKPAdjustment()
        {
            this.Icon = new Icon(Configuration.Instance().GetIconPath());

            InitializeComponent();
        }

        private void DrawSquare(Graphics gfx, Pen pen, int top, int bottom, int left, int right)
        {
            gfx.DrawLine(pen, new Point(left, top), new Point(right, top));
            gfx.DrawLine(pen, new Point(left, top), new Point(left, bottom));
            gfx.DrawLine(pen, new Point(left, bottom), new Point(right, bottom));
            gfx.DrawLine(pen, new Point(right, top), new Point(right, bottom));
        }

        private void DKPAdjustment_Load(object sender, System.EventArgs e)
        {
            this.Refresh_Raiders();

            Classes.GetAllClasses().ToList().ForEach(x => Filters.Add(x, false));

            for (int i = 0; i < NUMBER_SHOWN; i++)
            {
                void selectRaider(string x)
                {
                    var index = Raider_Combobox.FindStringExact(x);
                    Raider_Combobox.SelectedIndex = index;
                }

                var panel = new RaiderPanel(this, selectRaider, 180, 40 + i * 35);

                RaiderPanels.Add(panel);
            }

            this.ShowHighestDKP();
        }

        private void DKPAdjustment_Paint(object sender, PaintEventArgs e)
        {
            const int top = 10;
            const int header_height = 35;
            const int line_height = 35;

            const int bottom_filters = top + header_height + (line_height * 9);
            const int left_filters = 10;
            const int right_filters = 160;

            const int bottom_choices = top + header_height + (line_height * 10);
            const int left_choices = 170;
            const int right_choices = 515;

            const int left_adjust = 525;
            const int right_adjust = 795;
            const int bottom_adjust = 100;

            Graphics gfx = e.Graphics;
            Pen pen = new Pen(Color.DarkGray, 3);

            // Filters
            this.DrawSquare(gfx, pen, top, bottom_filters, left_filters, right_filters);

            // Choices
            this.DrawSquare(gfx, pen, top, bottom_choices, left_choices, right_choices);

            // Adjust
            this.DrawSquare(gfx, pen, top, bottom_adjust, left_adjust, right_adjust);
        }

        private void Class_Filter_Click(object sender, System.EventArgs e)
        {
            var button = sender as Button;

            var className = this.FindClassFromString(button.Name);

            this.Toggle_Filter(className);

            this.Refresh();
        }

        private void Toggle_Filter(string className)
        {
            if (className != null)
            {
                Filters[className] = !Filters[className];
            }

            this.ShowHighestDKP();
        }

        private string FindClassFromString(string Name)
        {
            foreach (var filter in Filters)
            {
                if (Name.Contains(filter.Key, StringComparison.InvariantCultureIgnoreCase))
                {
                    return filter.Key;
                }
            }

            throw new Exception("Filter not found");
        }

        private void Class_Filter_Paint(object sender, PaintEventArgs e)
        {
            var button = sender as Button;

            var className = this.FindClassFromString(button.Name);

            var stateText = Filters[className] ? "HIDE" : "SHOW";
            button.Text = $"{className} : {stateText}";

            Graphics gfx = e.Graphics;

            var stateColor = Filters[className] ? Color.Red : Color.Green;
            Pen pen = new Pen(stateColor, 2);

            this.DrawSquare(gfx, pen, 2, button.Height - 2, 2, button.Width - 2);
        }

        private void ShowHighestDKP()
        {
            //Refresh_Button_Click(null, null);

            var classFilteredRaiders = Raiders.Where(x => !Filters[x.Key.RClass]);

            var sortedRaiders = classFilteredRaiders.ToList();

            sortedRaiders.Sort(SortByDKP);

            var offset = Page * NUMBER_SHOWN;

            for(int i = 0; i < NUMBER_SHOWN; i++)
            {
                if(i + offset >= sortedRaiders.Count)
                {
                    RaiderPanels[i].Hide();

                    continue;
                }

                RaiderPanels[i].Show(sortedRaiders[i + offset]);
            }
        }

        public static int SortByDKP(KeyValuePair<Raider, int> x, KeyValuePair<Raider, int> y)
        {
            if (x.Value == y.Value)
            {
                return 0;
            }

            if (x.Value > y.Value)
            {
                return -1;
            }

            return 1;
        }

        private void Adjust_DKP_Button_Click(object sender, EventArgs e)
        {
            int dkp = Decimal.ToInt32(Adjust_DKP_Numeric.Value);

            if (Sign_Checkbox.Checked)
            {
                dkp = -dkp;
            }

            var raider = DatabaseManager.RaidersDatabase().Get(Raider_Combobox.Text);

            var reason = Common.GetReason();

            this.AdjustDKP(raider, dkp, reason, true);
        }

        private void AdjustDKP(Raider raider, int dkp, string reason, bool isAdjustment)
        {
            if (reason == null)
            {
                return;
            }

            DatabaseManager.DKPAuditLogsDatabase().Create(new DKPAuditLog(raider.Id, dkp, reason, isAdjustment));

            DatabaseManager.RaidersDatabase().AddDKP(raider, dkp);

            this.Quick_Refresh(raider, dkp);

            ShowHighestDKP();

        }

        private void Show_All_Button_Click(object sender, EventArgs e)
        {
            var filters = Filters.ToList();

            foreach(var filter in filters)
            {
                Filters[filter.Key] = false;
            }

            this.ShowHighestDKP();
            this.Refresh();
        }

        private void Hide_All_Button_Click(object sender, EventArgs e)
        {
            var filters = Filters.ToList();
            
            foreach (var filter in filters)
            {
                Filters[filter.Key] = true;
            }
            
            this.ShowHighestDKP();
            this.Refresh();
        }

        private void Refresh_Button_Click(object sender, EventArgs e)
        {
            this.Refresh_Raiders();

            this.ShowHighestDKP();
        }

        private void Refresh_Raiders()
        {
            Raiders.Clear();

            var raiders = DatabaseManager.RaidersDatabase().GetAll().ToList();

            foreach (var raider in raiders)
            {
                Raiders.Add(raider, DatabaseManager.DKPAuditLogsDatabase().GetDKP(raider));
            }

            var names = Raiders.Select(x => x.Key.Name);

            Raider_Combobox.Items.Clear();
            Raider_Combobox.Items.AddRange(names.ToArray());
            Raider_Combobox.Sorted = true;
        }

        private void Quick_Refresh(Raider raider, int dkp)
        {
            this.Raiders[raider] += dkp;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Page += 1;
            this.Page_Label.Text = Page.ToString();
            this.ShowHighestDKP();
        }

        private void Previous_Button_Click(object sender, EventArgs e)
        {
            this.Page -= 1;

            if(this.Page < 0)
            {
                this.Page = 0;
            }

            this.Page_Label.Text = this.Page.ToString();
            this.ShowHighestDKP();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
