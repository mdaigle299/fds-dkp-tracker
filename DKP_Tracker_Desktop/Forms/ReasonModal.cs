﻿using DKP_Tracker_Repository.Configuration;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace DKP_Tracker_Desktop.Forms
{
    public partial class ReasonModal : Form
    {
        public bool Cancelled = true;

        public ReasonModal()
        {
            this.Icon = new System.Drawing.Icon(Configuration.Instance().GetIconPath());

            InitializeComponent();
        }

        public string GetReason()
        {
            return Reason_Text.Text;
        }

        private void Cofirm_Button_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(Reason_Text.Text))
            {
                Feedback("You need to provide a reason that is not empty", Color.DarkRed);
            }
            else
            {
                Cancelled = false;
                Close();
            }
        }

        private void Cancel_Button_Click(object sender, EventArgs e)
        {
            Cancelled = true;
            Close();
        }

        private void Feedback(string message, Color color)
        {
            Feedback_Label.Text = message;
            Feedback_Label.ForeColor = color;
        }
    }
}
