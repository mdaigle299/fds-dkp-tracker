﻿namespace DKP_Tracker_Desktop.Forms
{
    partial class AuditLogReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Report_Text = new System.Windows.Forms.RichTextBox();
            this.Refresh_Button = new System.Windows.Forms.Button();
            this.Dates_Calendar = new System.Windows.Forms.MonthCalendar();
            this.label1 = new System.Windows.Forms.Label();
            this.OnlyAdjustment_CB = new System.Windows.Forms.CheckBox();
            this.Copy_Button = new System.Windows.Forms.Button();
            // 
            // Report_Text
            // 
            this.Report_Text.Location = new System.Drawing.Point(11, 214);
            this.Report_Text.Name = "Report_Text";
            this.Report_Text.ReadOnly = true;
            this.Report_Text.Size = new System.Drawing.Size(579, 507);
            this.Report_Text.TabIndex = 0;
            this.Report_Text.Text = "";
            // 
            // Refresh_Button
            // 
            this.Refresh_Button.Location = new System.Drawing.Point(440, 177);
            this.Refresh_Button.Name = "Refresh_Button";
            this.Refresh_Button.Size = new System.Drawing.Size(150, 25);
            this.Refresh_Button.TabIndex = 1;
            this.Refresh_Button.Text = "Refresh";
            this.Refresh_Button.UseVisualStyleBackColor = true;
            this.Refresh_Button.Click += new System.EventHandler(this.Refresh_Button_Click);
            // 
            // Dates_Calendar
            // 
            this.Dates_Calendar.Location = new System.Drawing.Point(11, 40);
            this.Dates_Calendar.MaxSelectionCount = 31;
            this.Dates_Calendar.Name = "Dates_Calendar";
            this.Dates_Calendar.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(11, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 21);
            this.label1.TabIndex = 3;
            this.label1.Text = "Filters";
            // 
            // OnlyAdjustment_CB
            // 
            this.OnlyAdjustment_CB.AutoSize = true;
            this.OnlyAdjustment_CB.Checked = true;
            this.OnlyAdjustment_CB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OnlyAdjustment_CB.Location = new System.Drawing.Point(250, 40);
            this.OnlyAdjustment_CB.Name = "OnlyAdjustment_CB";
            this.OnlyAdjustment_CB.Size = new System.Drawing.Size(158, 19);
            this.OnlyAdjustment_CB.TabIndex = 4;
            this.OnlyAdjustment_CB.Text = "Include Only Adjustment";
            this.OnlyAdjustment_CB.UseVisualStyleBackColor = true;
            // 
            // Copy_Button
            // 
            this.Copy_Button.Location = new System.Drawing.Point(440, 727);
            this.Copy_Button.Name = "Copy_Button";
            this.Copy_Button.Size = new System.Drawing.Size(150, 25);
            this.Copy_Button.TabIndex = 1;
            this.Copy_Button.Text = "Copy To Clipboard";
            this.Copy_Button.UseVisualStyleBackColor = true;
            this.Copy_Button.Click += new System.EventHandler(this.Copy_Button_Click);
            // 
            // AuditLogReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 760);
            this.Controls.Add(this.OnlyAdjustment_CB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Dates_Calendar);
            this.Controls.Add(this.Refresh_Button);
            this.Controls.Add(this.Report_Text);
            this.Controls.Add(this.Copy_Button);
            this.Name = "AuditLogReport";
            this.Text = "AuditLogReport";

        }

        #endregion

        private System.Windows.Forms.RichTextBox Report_Text;
        private System.Windows.Forms.Button Refresh_Button;
        private System.Windows.Forms.MonthCalendar Dates_Calendar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox OnlyAdjustment_CB;
        private System.Windows.Forms.Button Copy_Button;
    }
}