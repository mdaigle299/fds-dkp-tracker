﻿using DKP_Tracker_Repository.Models;
using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using System.Drawing;
using DKP_Tracker_Repository.Databases;
using DKP_Tracker_Desktop;

namespace DKP_Tracker_Desktop.Forms
{
    public partial class Raiders : Form
    {
        public Raiders()
        {
            InitializeComponent();
        }

        private void Add_Raider_Button_Click(object sender, EventArgs e)
        {
            if (DatabaseManager.RaidersDatabase().Get(Name_Text.Text) != null)
            {
                Feedback($"Raider {Name_Text.Text} already exists!", Color.DarkRed);
                return;
            }

            this.CreateRaider();

            if(DKP_Spinner.Value > 0)
            {
                this.AdjustDKPWithReason();
            }
        }

        private void Raiders_Load(object sender, EventArgs e)
        {
            var orderedList = Classes.GetAllClasses().OrderBy(x => x);

            Class_Combo.Items.AddRange(orderedList.ToArray<string>());

            Class_Combo.SelectedIndex = 0;
        }

        private void CreateRaider()
        {
            DatabaseManager.RaidersDatabase().Create(new Raider(Name_Text.Text, Class_Combo.SelectedItem.ToString()));

            Feedback($"Raider {Name_Text.Text} was created!", Color.DarkGreen);
        }

        private void AdjustDKPWithReason()
        {
            var reason = Common.GetReason();

            var raider = DatabaseManager.RaidersDatabase().Get(Name_Text.Text);

            AdjustDKP(raider, Decimal.ToInt32(DKP_Spinner.Value), reason, false);
        }

        private void AdjustDKP(Raider raider, int dkp, string reason, bool isAdjustment)
        {
            if (reason == null)
            {
                return;
            }
            
            DatabaseManager.DKPAuditLogsDatabase().Create(new DKPAuditLog(raider.Id, dkp, reason, isAdjustment));

            DatabaseManager.RaidersDatabase().AddDKP(raider, dkp);
        }

        private void Feedback(string message, Color color)
        {            
            Feedback_Label.Text = message;
            Feedback_Label.ForeColor = color;
        }
    }
}
