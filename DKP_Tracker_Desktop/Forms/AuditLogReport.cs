﻿using DKP_Tracker_Desktop.Reports;
using DKP_Tracker_Repository.Configuration;
using DKP_Tracker_Repository.Databases;
using System;
using System.Windows.Forms;

namespace DKP_Tracker_Desktop.Forms
{
    public partial class AuditLogReport : Form
    {
        public AuditLogReport()
        {
            this.Icon = new System.Drawing.Icon(Configuration.Instance().GetIconPath());
            
            InitializeComponent();
        }

        private void Copy_Button_Click(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(Report_Text.Text))
            {
                Clipboard.SetText(Report_Text.Text);
            }
        }

        private void Refresh_Button_Click(object sender, System.EventArgs e)
        {
            ApplyFilters();
        }

        private void ApplyFilters()
        {
            var onlyAdjustments = OnlyAdjustment_CB.Checked;
            var startDate = Dates_Calendar.SelectionStart.Date;
            var endDate = Dates_Calendar.SelectionEnd.Date.AddDays(1);

            var logs = DatabaseManager.DKPAuditLogsDatabase().GetLogsByDate(startDate, endDate, onlyAdjustments);
            var raiders = DatabaseManager.RaidersDatabase().GetAll();

            Report_Text.Text = DKPReports.CreateDatedReport(logs, raiders, startDate, endDate);
        }
    }
}
