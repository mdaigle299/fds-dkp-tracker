﻿using DKP_Tracker_Repository.Models;
using DKP_Tracker_Repository.Configuration;
using DKP_Tracker_Repository.Databases;
using System;
using System.Linq;
using System.Windows.Forms;
using DKP_Tracker_Desktop.Reports;

namespace DKP_Tracker_Desktop.Forms
{
    public partial class RaidersReport : Form
    {
        const string OrderByClass = "Class";

        const string OrderByName = "Name";

        public RaidersReport()
        {
            this.Icon = new System.Drawing.Icon(Configuration.Instance().GetIconPath());

            InitializeComponent();

            RefreshRaiders();
        }

        private void RefreshRaiders()
        {
            if(Raiders_List.SelectedIndex != -1)
            { 
                Raiders_List.SetSelected(Raiders_List.SelectedIndex, false);
            }

            AuditLogs_Text.Clear();
            Raiders_List.Items.Clear();

            var raiders = DatabaseManager.RaidersDatabase().GetAll().ToList();

            if (Order_Combobox.SelectedItem as string == OrderByClass)
            {
                raiders.Sort(delegate (Raider x, Raider y)
                {
                    return x.RClass.CompareTo(y.RClass);
                });
            }
            else
            {
                raiders.Sort(delegate (Raider x, Raider y)
                {
                    return x.Name.CompareTo(y.Name);
                });
            }

            foreach(var raider in raiders)
            {
                Raiders_List.Items.Add(raider);
            }
        }

        private void Copy_Button_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(AuditLogs_Text.Text))
            { 
                Clipboard.SetText(AuditLogs_Text.Text);
            }
        }

        private void Raiders_List_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Raiders_List.SelectedIndex == -1)
            {
                return;
            }

            var raiderOld = Raiders_List.Items[Raiders_List.SelectedIndex] as Raider;

            var raider = DatabaseManager.RaidersDatabase().Get(raiderOld.Id);
            var logs = DatabaseManager.DKPAuditLogsDatabase().GetLogsByRaider(raider);

            AuditLogs_Text.Text = DKPReports.CreateDetailedDKPReport(raider, logs);
        }

        private void Refresh_Button_Click(object sender, EventArgs e)
        {
            RefreshRaiders();
        }

        private void RaidersReport_Load(object sender, EventArgs e)
        {
            Order_Combobox.Items.Add(OrderByName);
            Order_Combobox.Items.Add(OrderByClass);

            Order_Combobox.SelectedItem = OrderByName;
        }

        private void Order_Combobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshRaiders();
        }
    }
}
