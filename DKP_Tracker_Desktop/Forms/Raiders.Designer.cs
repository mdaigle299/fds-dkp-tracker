﻿namespace DKP_Tracker_Desktop.Forms
{
    partial class Raiders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Class_Combo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Name_Text = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Add_Raider_Button = new System.Windows.Forms.Button();
            this.DKP_Spinner = new System.Windows.Forms.NumericUpDown();
            this.Feedback_Label = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DKP_Spinner)).BeginInit();
            // 
            // Class_Combo
            // 
            this.Class_Combo.FormattingEnabled = true;
            this.Class_Combo.Location = new System.Drawing.Point(90, 89);
            this.Class_Combo.Name = "Class_Combo";
            this.Class_Combo.Size = new System.Drawing.Size(135, 23);
            this.Class_Combo.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // Name_Text
            // 
            this.Name_Text.Location = new System.Drawing.Point(90, 31);
            this.Name_Text.Name = "Name_Text";
            this.Name_Text.Size = new System.Drawing.Size(135, 23);
            this.Name_Text.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-23, -46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "DKP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Class";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(13, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 21);
            this.label5.TabIndex = 7;
            this.label5.Text = "Raider";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(-1591, 55);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Add_Raider_Button
            // 
            this.Add_Raider_Button.Location = new System.Drawing.Point(90, 118);
            this.Add_Raider_Button.Name = "Add_Raider_Button";
            this.Add_Raider_Button.Size = new System.Drawing.Size(135, 23);
            this.Add_Raider_Button.TabIndex = 4;
            this.Add_Raider_Button.Text = "Add Raider";
            this.Add_Raider_Button.UseVisualStyleBackColor = true;
            this.Add_Raider_Button.Click += new System.EventHandler(this.Add_Raider_Button_Click);
            // 
            // DKP_Spinner
            // 
            this.DKP_Spinner.AutoSize = true;
            this.DKP_Spinner.Location = new System.Drawing.Point(90, 60);
            this.DKP_Spinner.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.DKP_Spinner.Name = "DKP_Spinner";
            this.DKP_Spinner.Size = new System.Drawing.Size(135, 23);
            this.DKP_Spinner.TabIndex = 2;
            // 
            // Feedback_Label
            // 
            this.Feedback_Label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.Feedback_Label.Location = new System.Drawing.Point(14, 151);
            this.Feedback_Label.Name = "Feedback_Label";
            this.Feedback_Label.Size = new System.Drawing.Size(211, 17);
            this.Feedback_Label.TabIndex = 9;
            this.Feedback_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(13, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(212, 23);
            this.label6.TabIndex = 10;
            this.label6.Text = "_________________________________________________________";
            // 
            // Raiders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(235, 175);
            this.Controls.Add(this.Feedback_Label);
            this.Controls.Add(this.DKP_Spinner);
            this.Controls.Add(this.Add_Raider_Button);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Name_Text);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Class_Combo);
            this.Controls.Add(this.label6);
            this.Name = "Raiders";
            this.Text = "Raiders";
            this.Load += new System.EventHandler(this.Raiders_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DKP_Spinner)).EndInit();

        }

        #endregion

        private System.Windows.Forms.ComboBox Class_Combo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Name_Text;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Add_Raider_Button;
        private System.Windows.Forms.NumericUpDown DKP_Spinner;
        private System.Windows.Forms.Label Feedback_Label;
        private System.Windows.Forms.Label label6;
    }
}