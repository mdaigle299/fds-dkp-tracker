﻿using DKP_Tracker_Repository.Models;
using DKP_Tracker_Desktop.Utils;
using DKP_Tracker_Repository.Configuration;
using DKP_Tracker_Repository.Databases;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DKP_Tracker_Desktop;
using DKP_Tracker_Desktop.Reports;

namespace DKP_Tracker_Desktop.Forms
{
    public partial class MainForm : Form
    {
        private bool Refreshing = false;   

        public MainForm()
        {
            this.Icon = new System.Drawing.Icon(Configuration.Instance().GetIconPath());
            this.Paint += new PaintEventHandler(Main_Form_Paint);

            InitializeComponent();
        }

        private void Full_DKP_Report_Button_Click(object sender, System.EventArgs e)
        {
            using ReportModal reportModal = new ReportModal();
            
            reportModal.ShowDialog();

            if (reportModal.Cancelled == false)
            {
                var report = reportModal.GetReportString();

                report = DKPReports.CreateReport(report, this.GetCurrentRaiders());

                if (!string.IsNullOrEmpty(report))
                {
                    Clipboard.SetText(report);
                }
            }
        }

        private void Current_DKP_Report_Button_Click(object sender, System.EventArgs e)
        {
            var report = DKPReports.CreateReport("%Name% ● %Class% ● %Dkp%", DatabaseManager.RaidersDatabase().GetAll());

            if (!string.IsNullOrEmpty(report))
            {
                Clipboard.SetText(report);
            }
        }

        private IEnumerable<Raider> GetCurrentRaiders()
        {
            var currentRaiders = this.All_Raider_List.CheckedItems;

            var raiders = new List<Raider>(40);

            foreach(var currentRaider in currentRaiders)
            {
                if (currentRaider is Raider raider)
                {
                    raiders.Add(raider);
                }
            }
            return raiders;
        }

        private void Give_DKP_To_Raiders_Click(object sender, EventArgs e)
        {
            int dkp = Decimal.ToInt32(Current_Raid_DKP_Numeric.Value);

            var raiders = this.GetCurrentRaiders();

            var reason = Common.GetReason();

            if (reason != null)
            {

                foreach (var raider in raiders)
                {
                    this.CreateAuditLog(raider, dkp, reason, false);
                }

                DatabaseManager.RaidersDatabase().AddDKPBatch(raiders.ToList(), dkp);
            }
        }

        private void CreateAuditLog(Raider raider, int dkp, string reason, bool isAdjustment)
        {
            if(reason == null)
            {
                return;
            }
            
            DatabaseManager.DKPAuditLogsDatabase().Create(new DKPAuditLog(raider.Id, dkp, reason, isAdjustment));

            this.Current_Raid_DKP_Numeric.Value = 0;
        }

        private void Get_Detailed_Report_Click(object sender, EventArgs e)
        {
            var raidersReports = new RaidersReport();

            raidersReports.Show();
        }

        private void Audit_Log_Report_Button_Click(object sender, EventArgs e)
        {
            var auditLogReport = new AuditLogReport();
            
            auditLogReport.Show(this);

        }

        private void Raider_Button_Click(object sender, EventArgs e)
        {
            Raiders raider = new Raiders
            {
                Visible = true
            };
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.RefreshAll();
        }

        private void Refresh_Button_Click(object sender, EventArgs e)
        {
            this.RefreshAll();
        }

        private void RefreshAll()
        {
            this.Refreshing = true;
            var raiders = DatabaseManager.RaidersDatabase().GetAll().ToArray();
            
            All_Raider_List.Items.Clear();

            All_Raider_List.Items.AddRange(raiders);

            All_Raider_List.Sorted = true;

            this.LoadCurrentRaiders();

            Refresh();
            this.Refreshing = false;
        }
               
        private void LoadCurrentRaiders()
        {
            var selectedRaiders = JsonUtils.LoadFromJson("SelectedRaiders");

            if (selectedRaiders.Count > 0)
            {
                foreach (var selectedRaider in selectedRaiders)
                {
                    var index = All_Raider_List.Items.IndexOf(selectedRaider);

                    if (index != -1)
                    {
                        All_Raider_List.SetItemChecked(index, true);
                    }
                }
            }
        }

        private void Main_Form_Paint(object sender, PaintEventArgs e)
        {
            int left = 275;
            int top = 40;
            int bottom = 90;
            int right = 555;

            int top_adjust = 345;
            int bottom_adjust = 420;

            int top_reports = 115;
            int bottom_reports = 320;

            Graphics gfx = e.Graphics;
            Pen p = new Pen(Color.DarkGray, 3);

            /// Current Raid Group
            gfx.DrawLine(p, new Point(left, top), new Point(right, top));
            gfx.DrawLine(p, new Point(left, top), new Point(left, bottom));
            gfx.DrawLine(p, new Point(left, bottom), new Point(right, bottom));
            gfx.DrawLine(p, new Point(right, top), new Point(right, bottom));
            
            /// Adjust Group
            gfx.DrawLine(p, new Point(left, top_adjust), new Point(right, top_adjust));
            gfx.DrawLine(p, new Point(left, top_adjust), new Point(left, bottom_adjust));
            gfx.DrawLine(p, new Point(left, bottom_adjust), new Point(right, bottom_adjust));
            gfx.DrawLine(p, new Point(right, top_adjust), new Point(right, bottom_adjust));

            /// Reports
            gfx.DrawLine(p, new Point(left, top_reports), new Point(right, top_reports));
            gfx.DrawLine(p, new Point(left, top_reports), new Point(left, bottom_reports));
            gfx.DrawLine(p, new Point(left, bottom_reports), new Point(right, bottom_reports));
            gfx.DrawLine(p, new Point(right, top_reports), new Point(right, bottom_reports));
        }

        private void All_Raider_List_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if(!this.Refreshing)
            {
                SaveSelectedRaiders(e);
            }

            var adjust = e.NewValue == CheckState.Unchecked ? -1 : 1;

            Selected_Raider_Label.Text = $"Selected Raider : {All_Raider_List.CheckedItems.Count + adjust}";
        }

        private void Clear_List_Button_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < All_Raider_List.Items.Count; i++)
            {
                All_Raider_List.SetItemChecked(i, false);
            }

            SaveSelectedRaiders(null);
        }

        private void SaveSelectedRaiders(ItemCheckEventArgs e)
        {
            var raiders = All_Raider_List.CheckedItems;
            var raidersList = new List<string>();

            foreach (var raider in raiders)
            {
                raidersList.Add((raider as Raider).ToString());
            }

            if(e != null)
            { 
                if (e.NewValue == CheckState.Unchecked)
                {
                    raidersList.Remove((All_Raider_List.Items[e.Index] as Raider).ToString());
                }
                else if (e.NewValue == CheckState.Checked)
                {
                    raidersList.Add((All_Raider_List.Items[e.Index] as Raider).ToString());
                }
            }

            JsonUtils.SaveToJson(raidersList, "SelectedRaiders");
        }

        private void Adjustment_Window_Click(object sender, EventArgs e)
        {
            var dkpAdjustment = new DKPAdjustment();

            dkpAdjustment.Show();
        }

        private void Load_from_clipboard_Click(object sender, EventArgs e)
        {
            var splitText = Clipboard.GetText().Split(' ', '\r', '\n');

            Clear_List_Button_Click(sender, e);
            
            foreach (var text in splitText)
            {
                var select = All_Raider_List.Items.OfType<Raider>().Where(x => x.Name == text);
                if (select.Any())
                {
                    var index = All_Raider_List.Items.IndexOf(select.First());

                    All_Raider_List.SetItemChecked(index, true);
                }
            }
        }
    }
}
