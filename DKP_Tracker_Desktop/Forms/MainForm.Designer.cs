﻿namespace DKP_Tracker_Desktop.Forms
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Create_Report_Button = new System.Windows.Forms.Button();
            this.Current_Line_Up_Label = new System.Windows.Forms.Label();
            this.Reports_Label = new System.Windows.Forms.Label();
            this.Clipboard_Notice_Label = new System.Windows.Forms.Label();
            this.Give_DKP_To_Raiders = new System.Windows.Forms.Button();
            this.Adjustment_Label = new System.Windows.Forms.Label();
            this.Get_Detailed_Report = new System.Windows.Forms.Button();
            this.Audit_Log_Report_Button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Raider_Button = new System.Windows.Forms.Button();
            this.All_Raider_List = new System.Windows.Forms.CheckedListBox();
            this.Refresh_Button = new System.Windows.Forms.Button();
            this.Current_Raid_DKP_Numeric = new System.Windows.Forms.NumericUpDown();
            this.Current_Raid_Positive_Radio_Button = new System.Windows.Forms.RadioButton();
            this.Current_Raid_Negative_Radio_Button = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.Clear_List_Button = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Selected_Raider_Label = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Current_Raid_DKP_Numeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            // 
            // Create_Report_Button
            // 
            this.Create_Report_Button.Location = new System.Drawing.Point(292, 150);
            this.Create_Report_Button.Name = "Create_Report_Button";
            this.Create_Report_Button.Size = new System.Drawing.Size(141, 23);
            this.Create_Report_Button.TabIndex = 2;
            this.Create_Report_Button.Text = "Create Report";
            this.Create_Report_Button.UseVisualStyleBackColor = true;
            this.Create_Report_Button.Click += new System.EventHandler(this.Full_DKP_Report_Button_Click);
            // 
            // Current_Line_Up_Label
            // 
            this.Current_Line_Up_Label.AutoSize = true;
            this.Current_Line_Up_Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Current_Line_Up_Label.Location = new System.Drawing.Point(5, 8);
            this.Current_Line_Up_Label.Name = "Current_Line_Up_Label";
            this.Current_Line_Up_Label.Size = new System.Drawing.Size(160, 21);
            this.Current_Line_Up_Label.TabIndex = 4;
            this.Current_Line_Up_Label.Text = "Current raid line-up";
            // 
            // Reports_Label
            // 
            this.Reports_Label.AutoSize = true;
            this.Reports_Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Reports_Label.Location = new System.Drawing.Point(288, 100);
            this.Reports_Label.Name = "Reports_Label";
            this.Reports_Label.Size = new System.Drawing.Size(68, 21);
            this.Reports_Label.TabIndex = 4;
            this.Reports_Label.Text = "Reports";
            // 
            // Clipboard_Notice_Label
            // 
            this.Clipboard_Notice_Label.AutoSize = true;
            this.Clipboard_Notice_Label.Location = new System.Drawing.Point(293, 132);
            this.Clipboard_Notice_Label.Name = "Clipboard_Notice_Label";
            this.Clipboard_Notice_Label.Size = new System.Drawing.Size(249, 15);
            this.Clipboard_Notice_Label.TabIndex = 5;
            this.Clipboard_Notice_Label.Text = "These reports copy to clipboard automatically";
            // 
            // Give_DKP_To_Raiders
            // 
            this.Give_DKP_To_Raiders.Location = new System.Drawing.Point(429, 57);
            this.Give_DKP_To_Raiders.Name = "Give_DKP_To_Raiders";
            this.Give_DKP_To_Raiders.Size = new System.Drawing.Size(111, 23);
            this.Give_DKP_To_Raiders.TabIndex = 0;
            this.Give_DKP_To_Raiders.Text = "Add DKP";
            this.Give_DKP_To_Raiders.UseVisualStyleBackColor = true;
            this.Give_DKP_To_Raiders.Click += new System.EventHandler(this.Give_DKP_To_Raiders_Click);
            // 
            // Adjustment_Label
            // 
            this.Adjustment_Label.AutoSize = true;
            this.Adjustment_Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Adjustment_Label.Location = new System.Drawing.Point(288, 330);
            this.Adjustment_Label.Name = "Adjustment_Label";
            this.Adjustment_Label.Size = new System.Drawing.Size(99, 21);
            this.Adjustment_Label.TabIndex = 4;
            this.Adjustment_Label.Text = "Adjustment";
            // 
            // Get_Detailed_Report
            // 
            this.Get_Detailed_Report.Location = new System.Drawing.Point(293, 223);
            this.Get_Detailed_Report.Name = "Get_Detailed_Report";
            this.Get_Detailed_Report.Size = new System.Drawing.Size(141, 23);
            this.Get_Detailed_Report.TabIndex = 2;
            this.Get_Detailed_Report.Text = "Raiders DKP Reports";
            this.Get_Detailed_Report.UseVisualStyleBackColor = true;
            this.Get_Detailed_Report.Click += new System.EventHandler(this.Get_Detailed_Report_Click);
            // 
            // Audit_Log_Report_Button
            // 
            this.Audit_Log_Report_Button.Location = new System.Drawing.Point(293, 252);
            this.Audit_Log_Report_Button.Name = "Audit_Log_Report_Button";
            this.Audit_Log_Report_Button.Size = new System.Drawing.Size(141, 23);
            this.Audit_Log_Report_Button.TabIndex = 2;
            this.Audit_Log_Report_Button.Text = "Audit Log Reports";
            this.Audit_Log_Report_Button.UseVisualStyleBackColor = true;
            this.Audit_Log_Report_Button.Click += new System.EventHandler(this.Audit_Log_Report_Button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(293, 205);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "These reports open a new window";
            // 
            // Raider_Button
            // 
            this.Raider_Button.Location = new System.Drawing.Point(177, 638);
            this.Raider_Button.Name = "Raider_Button";
            this.Raider_Button.Size = new System.Drawing.Size(85, 23);
            this.Raider_Button.TabIndex = 2;
            this.Raider_Button.Text = "Create Raider";
            this.Raider_Button.UseVisualStyleBackColor = true;
            this.Raider_Button.Click += new System.EventHandler(this.Raider_Button_Click);
            // 
            // All_Raider_List
            // 
            this.All_Raider_List.CheckOnClick = true;
            this.All_Raider_List.FormattingEnabled = true;
            this.All_Raider_List.Location = new System.Drawing.Point(9, 31);
            this.All_Raider_List.Name = "All_Raider_List";
            this.All_Raider_List.Size = new System.Drawing.Size(253, 580);
            this.All_Raider_List.TabIndex = 7;
            this.All_Raider_List.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.All_Raider_List_ItemCheck);
            // 
            // Refresh_Button
            // 
            this.Refresh_Button.Location = new System.Drawing.Point(9, 638);
            this.Refresh_Button.Name = "Refresh_Button";
            this.Refresh_Button.Size = new System.Drawing.Size(60, 23);
            this.Refresh_Button.TabIndex = 2;
            this.Refresh_Button.Text = "Refresh";
            this.Refresh_Button.UseVisualStyleBackColor = true;
            this.Refresh_Button.Click += new System.EventHandler(this.Refresh_Button_Click);
            // 
            // Current_Raid_DKP_Numeric
            // 
            this.Current_Raid_DKP_Numeric.Location = new System.Drawing.Point(292, 57);
            this.Current_Raid_DKP_Numeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.Current_Raid_DKP_Numeric.Name = "Current_Raid_DKP_Numeric";
            this.Current_Raid_DKP_Numeric.Size = new System.Drawing.Size(112, 23);
            this.Current_Raid_DKP_Numeric.TabIndex = 0;
            // 
            // Current_Raid_Positive_Radio_Button
            // 
            this.Current_Raid_Positive_Radio_Button.Location = new System.Drawing.Point(0, 0);
            this.Current_Raid_Positive_Radio_Button.Name = "Current_Raid_Positive_Radio_Button";
            this.Current_Raid_Positive_Radio_Button.Size = new System.Drawing.Size(104, 24);
            this.Current_Raid_Positive_Radio_Button.TabIndex = 0;
            // 
            // Current_Raid_Negative_Radio_Button
            // 
            this.Current_Raid_Negative_Radio_Button.Location = new System.Drawing.Point(0, 0);
            this.Current_Raid_Negative_Radio_Button.Name = "Current_Raid_Negative_Radio_Button";
            this.Current_Raid_Negative_Radio_Button.Size = new System.Drawing.Size(104, 24);
            this.Current_Raid_Negative_Radio_Button.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(288, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "Current raid";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(227, 324);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(112, 23);
            this.numericUpDown1.TabIndex = 0;
            // 
            // Clear_List_Button
            // 
            this.Clear_List_Button.Location = new System.Drawing.Point(84, 638);
            this.Clear_List_Button.Name = "Clear_List_Button";
            this.Clear_List_Button.Size = new System.Drawing.Size(60, 23);
            this.Clear_List_Button.TabIndex = 2;
            this.Clear_List_Button.Text = "Clear";
            this.Clear_List_Button.UseVisualStyleBackColor = true;
            this.Clear_List_Button.Click += new System.EventHandler(this.Clear_List_Button_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(293, 354);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Adjustment Window";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Adjustment_Window_Click);
            // 
            // Selected_Raider_Label
            // 
            this.Selected_Raider_Label.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Selected_Raider_Label.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Selected_Raider_Label.Location = new System.Drawing.Point(102, 614);
            this.Selected_Raider_Label.Name = "Selected_Raider_Label";
            this.Selected_Raider_Label.Size = new System.Drawing.Size(160, 21);
            this.Selected_Raider_Label.TabIndex = 4;
            this.Selected_Raider_Label.Text = "Current raid line-up";
            this.Selected_Raider_Label.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(268, 638);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Load";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Load_from_clipboard_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 671);
            this.Controls.Add(this.Current_Raid_DKP_Numeric);
            this.Controls.Add(this.All_Raider_List);
            this.Controls.Add(this.Clipboard_Notice_Label);
            this.Controls.Add(this.Current_Line_Up_Label);
            this.Controls.Add(this.Create_Report_Button);
            this.Controls.Add(this.Reports_Label);
            this.Controls.Add(this.Give_DKP_To_Raiders);
            this.Controls.Add(this.Adjustment_Label);
            this.Controls.Add(this.Get_Detailed_Report);
            this.Controls.Add(this.Audit_Log_Report_Button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Raider_Button);
            this.Controls.Add(this.Refresh_Button);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Clear_List_Button);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Selected_Raider_Label);
            this.Controls.Add(this.button2);
            this.Name = "MainForm";
            this.Text = "DKP Tracker";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Current_Raid_DKP_Numeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();

        }

        #endregion
        private System.Windows.Forms.Button Create_Report_Button;
        private System.Windows.Forms.Label Current_Line_Up_Label;
        private System.Windows.Forms.Label Reports_Label;
        private System.Windows.Forms.Label Clipboard_Notice_Label;
        private System.Windows.Forms.Button Give_DKP_To_Raiders;
        private System.Windows.Forms.Label Adjustment_Label;
        private System.Windows.Forms.Button Get_Detailed_Report;
        private System.Windows.Forms.Button Audit_Log_Report_Button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Raider_Button;
        private System.Windows.Forms.CheckedListBox All_Raider_List;
        private System.Windows.Forms.Button Refresh_Button;
        private System.Windows.Forms.NumericUpDown Current_Raid_DKP_Numeric;
        private System.Windows.Forms.RadioButton Current_Raid_Positive_Radio_Button;
        private System.Windows.Forms.RadioButton Current_Raid_Negative_Radio_Button;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button Clear_List_Button;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label Selected_Raider_Label;
        private System.Windows.Forms.Button button2;
    }
}

