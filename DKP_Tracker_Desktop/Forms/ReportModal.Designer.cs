﻿namespace DKP_Tracker_Desktop.Forms
{
    partial class ReportModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Reason_Label = new System.Windows.Forms.Label();
            this.Confirm_Button = new System.Windows.Forms.Button();
            this.Reason_Text = new System.Windows.Forms.TextBox();
            this.Cancel_Button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Feedback_Label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            // 
            // Reason_Label
            // 
            this.Reason_Label.AutoSize = true;
            this.Reason_Label.Location = new System.Drawing.Point(12, 13);
            this.Reason_Label.Name = "Reason_Label";
            this.Reason_Label.Size = new System.Drawing.Size(75, 15);
            this.Reason_Label.TabIndex = 1;
            this.Reason_Label.Text = "Report string";
            // 
            // Confirm_Button
            // 
            this.Confirm_Button.Location = new System.Drawing.Point(12, 60);
            this.Confirm_Button.Name = "Confirm_Button";
            this.Confirm_Button.Size = new System.Drawing.Size(75, 23);
            this.Confirm_Button.TabIndex = 2;
            this.Confirm_Button.Text = "Confirm";
            this.Confirm_Button.UseVisualStyleBackColor = true;
            this.Confirm_Button.Click += new System.EventHandler(this.Cofirm_Button_Click);
            // 
            // Reason_Text
            // 
            this.Reason_Text.Location = new System.Drawing.Point(12, 31);
            this.Reason_Text.Name = "Reason_Text";
            this.Reason_Text.Size = new System.Drawing.Size(410, 23);
            this.Reason_Text.TabIndex = 3;
            this.Reason_Text.Text = "%OrderByClass%%Name% ● %Dkp%";
            this.Reason_Text.TextChanged += new System.EventHandler(this.Reason_Text_TextChanged);
            // 
            // Cancel_Button
            // 
            this.Cancel_Button.Location = new System.Drawing.Point(347, 60);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(75, 23);
            this.Cancel_Button.TabIndex = 4;
            this.Cancel_Button.Text = "Cancel";
            this.Cancel_Button.UseVisualStyleBackColor = true;
            this.Cancel_Button.Click += new System.EventHandler(this.Cancel_Button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 184);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(412, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "_________________________________________________________________________________" +
    "";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Feedback_Label
            // 
            this.Feedback_Label.Location = new System.Drawing.Point(12, 208);
            this.Feedback_Label.Name = "Feedback_Label";
            this.Feedback_Label.Size = new System.Drawing.Size(412, 15);
            this.Feedback_Label.TabIndex = 5;
            this.Feedback_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Report Variables";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 60);
            this.label3.TabIndex = 1;
            this.label3.Text = "Order By\r\nNo order variable = Name\r\n%OrderByClass%\r\n%OrderByDkp%\r\n";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(206, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 60);
            this.label4.TabIndex = 1;
            this.label4.Text = "%Name%\r\n%Dkp%\r\n%Class%\r\n%Presence%\r\n";
            // 
            // ReportModal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 224);
            this.Controls.Add(this.Cancel_Button);
            this.Controls.Add(this.Reason_Text);
            this.Controls.Add(this.Confirm_Button);
            this.Controls.Add(this.Reason_Label);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Feedback_Label);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Name = "ReportModal";
            this.Text = "Provide your report string";

        }

        #endregion
        private System.Windows.Forms.Label Reason_Label;
        private System.Windows.Forms.Button Confirm_Button;
        private System.Windows.Forms.TextBox Reason_Text;
        private System.Windows.Forms.Button Cancel_Button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Feedback_Label;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}