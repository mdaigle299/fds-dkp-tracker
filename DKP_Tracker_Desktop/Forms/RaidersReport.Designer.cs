﻿namespace DKP_Tracker_Desktop.Forms
{
    partial class RaidersReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Raiders_List = new System.Windows.Forms.ListBox();
            this.AuditLogs_Text = new System.Windows.Forms.RichTextBox();
            this.Copy_Button = new System.Windows.Forms.Button();
            this.Refresh_Button = new System.Windows.Forms.Button();
            this.Order_Combobox = new System.Windows.Forms.ComboBox();
            // 
            // Raiders_List
            // 
            this.Raiders_List.FormattingEnabled = true;
            this.Raiders_List.ItemHeight = 15;
            this.Raiders_List.Location = new System.Drawing.Point(7, 36);
            this.Raiders_List.Name = "Raiders_List";
            this.Raiders_List.Size = new System.Drawing.Size(150, 514);
            this.Raiders_List.TabIndex = 0;
            this.Raiders_List.SelectedIndexChanged += new System.EventHandler(this.Raiders_List_SelectedIndexChanged);
            // 
            // AuditLogs_Text
            // 
            this.AuditLogs_Text.Location = new System.Drawing.Point(163, 6);
            this.AuditLogs_Text.Name = "AuditLogs_Text";
            this.AuditLogs_Text.Size = new System.Drawing.Size(630, 513);
            this.AuditLogs_Text.TabIndex = 1;
            this.AuditLogs_Text.Text = "";
            // 
            // Copy_Button
            // 
            this.Copy_Button.Location = new System.Drawing.Point(643, 525);
            this.Copy_Button.Name = "Copy_Button";
            this.Copy_Button.Size = new System.Drawing.Size(150, 25);
            this.Copy_Button.TabIndex = 2;
            this.Copy_Button.Text = "Copy to Clipboard";
            this.Copy_Button.UseVisualStyleBackColor = true;
            this.Copy_Button.Click += new System.EventHandler(this.Copy_Button_Click);
            // 
            // Refresh_Button
            // 
            this.Refresh_Button.Location = new System.Drawing.Point(163, 525);
            this.Refresh_Button.Name = "Refresh_Button";
            this.Refresh_Button.Size = new System.Drawing.Size(150, 25);
            this.Refresh_Button.TabIndex = 2;
            this.Refresh_Button.Text = "Refresh Raiders";
            this.Refresh_Button.UseVisualStyleBackColor = true;
            this.Refresh_Button.Click += new System.EventHandler(this.Refresh_Button_Click);
            // 
            // Order_Combobox
            // 
            this.Order_Combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Order_Combobox.FormattingEnabled = true;
            this.Order_Combobox.ItemHeight = 15;
            this.Order_Combobox.Location = new System.Drawing.Point(7, 6);
            this.Order_Combobox.MaxDropDownItems = 2;
            this.Order_Combobox.Name = "Order_Combobox";
            this.Order_Combobox.Size = new System.Drawing.Size(150, 23);
            this.Order_Combobox.TabIndex = 3;
            this.Order_Combobox.SelectedIndexChanged += new System.EventHandler(this.Order_Combobox_SelectedIndexChanged);
            // 
            // RaidersReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 562);
            this.Controls.Add(this.Order_Combobox);
            this.Controls.Add(this.Copy_Button);
            this.Controls.Add(this.AuditLogs_Text);
            this.Controls.Add(this.Raiders_List);
            this.Controls.Add(this.Refresh_Button);
            this.Name = "RaidersReport";
            this.Text = "Raiders Report";
            this.Load += new System.EventHandler(this.RaidersReport_Load);

        }

        #endregion

        private System.Windows.Forms.ListBox Raiders_List;
        private System.Windows.Forms.RichTextBox AuditLogs_Text;
        private System.Windows.Forms.Button Copy_Button;
        private System.Windows.Forms.Button Refresh_Button;
        private System.Windows.Forms.ComboBox Order_Combobox;
    }
}