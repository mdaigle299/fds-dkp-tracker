﻿using DKP_Tracker_Repository.Databases;
using Microsoft.EntityFrameworkCore;

namespace DKP_Tracker_Migration
{
    class Program
    {
        static void Main()
        {
            using (var context = new RaidersDatabase())
            {
                context.Database.Migrate();
            }

            using (var context = new DKPAuditLogsDatabase())
            {
                context.Database.Migrate();
            }
        }
    }
}
